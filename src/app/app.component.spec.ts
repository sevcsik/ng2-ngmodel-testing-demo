/* tslint:disable:no-unused-variable */

import { TestBed, async, fakeAsync, tick } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { AppComponent } from './app.component';

describe('App: Ng2demo', () => {
  let fixture;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ FormsModule ],
      declarations: [ AppComponent ]
    });

    fixture = TestBed.createComponent(AppComponent);
  });

  it( 'should put the uppercased version of the input field\'s input into'
    + 'the code element', fakeAsync(() => {
    fixture.detectChanges();

    // put our test string to the input element
    let element = fixture.debugElement.query(By.css('input')).nativeElement;
    element.value = 'test';
    element.dispatchEvent(new Event('input'));

    tick();
    fixture.detectChanges();

    // expect it to be the uppercase version
    expect(fixture.debugElement
                .query(By.css('code'))
                .nativeElement
                .textContent
          ).toEqual('TEST');
  }));
});
